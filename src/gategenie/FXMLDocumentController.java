/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gategenie;

import com.gategenie.bl.Admin;
import com.gategenie.bl.User;
import com.jfoenix.controls.JFXTextField;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;


/**
 *
 * @author francesca
 */
public class FXMLDocumentController implements Initializable {
    static BufferedReader in= new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = new PrintStream(System.out);
    Event event;
    static ArrayList<Admin> admins = new ArrayList<Admin>();
    static String identification="";
    
    
    @FXML
    private Button loginButton;
    static JFXTextField identificationField= new JFXTextField();
    
    @FXML
    private Label label;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
               //                int popcion;
//        boolean salir = false;
        boolean usuarioAutorizado = false;
        testUsuario();
        do{ 
            try {
                usuarioAutorizado = login();
                
//            if(usuarioAutorizado == true){
//                out.println("***********************************************************"+"\n"+
//                            "#············Bienvenido(a) al portal del sistema············#"+"\n"+
//                            "##                FAVOR DIGITAR UNA OPCION                 ##"+"\n"+
//                            "***********************************************************");
//                do{
//               
//                    mostrarMenu();
//                    popcion= opcionIngresada();
//                    salir = procesarOpcion(popcion);
//
//                } while(salir);
//            }
            } catch (IOException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        } while (usuarioAutorizado==false);
    }



    
    
   
    //Login doctor--------------------------------------------------
    static void testUsuario(){
        Admin testingUsuario = new Admin("fem","FRANCESCA", "IZURIETA", "1111", "SDASDASD@ASDASD.COM", "sabanilla","costarican", LocalDate.now(),"31");
        admins.add(testingUsuario);
    }
    
    static boolean login() throws IOException{
        boolean loginAuthorized= false;
        identification = identificationField.getText();
        for(User dato : admins){
            if(identification.equals(dato.getId())){
                loginAuthorized= true;
            }else{
                out.println("usuario inválido o no registrado.");
            }
        }
        return loginAuthorized;
    }

    
}    
    
